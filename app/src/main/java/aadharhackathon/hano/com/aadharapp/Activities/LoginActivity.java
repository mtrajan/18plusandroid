package aadharhackathon.hano.com.aadharapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONException;
import org.json.JSONObject;

import Util.Constants;
import aadharhackathon.hano.com.aadharapp.R;


public class LoginActivity extends ActionBarActivity {

    ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        Button btVerify = (Button) findViewById(R.id.verifyButton);
        TextView tvRegister = (TextView) findViewById(R.id.registerButton);
        final EditText etMobileNumber = (EditText) findViewById(R.id.mobileNumberEditText);

        btVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((etMobileNumber.getText().toString().length() > 0) && etMobileNumber.getText().toString().length() == 10) {

                    mProgress.setVisibility(View.VISIBLE);
                    checkLogin(etMobileNumber.getText().toString().trim());

                } else {

                    Toast.makeText(getApplicationContext(),"Please enter 10 digit mobile no.",Toast.LENGTH_SHORT).show();

                }

            }
        });


        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                mIntent.putExtra(Constants.AADHAR_NUMBER, "sdf");
                startActivity(mIntent);
                //TODO take the user to register screen.


            }
        });

    }

    private void checkLogin(String mobileNumber) {

       String url = Constants.BASE_URL+Constants.IS_REGISTERED+mobileNumber;

        JsonObjectRequest checkLoginRequest = new JsonObjectRequest(Request.Method.GET,url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        mProgress.setVisibility(View.GONE);
                        try {
                            String registered = response.getString("registered");
                            if(registered.equals("true")) {

                                String aadharNumber = response.getString("aadhaar");

                                Intent fingerPrintIntent = new Intent(LoginActivity.this, FingerPrintScannerActivity.class);
                                fingerPrintIntent.putExtra(Constants.AADHAR_NUMBER, aadharNumber);
                                startActivity(fingerPrintIntent);

                            }else {

                                String aadharNumber = response.getString("aadhaar");
                                Intent fingerPrintIntent = new Intent(LoginActivity.this, FingerPrintScannerActivity.class);
                                fingerPrintIntent.putExtra(Constants.AADHAR_NUMBER, aadharNumber);
                                startActivity(fingerPrintIntent);

                                Toast.makeText(getApplicationContext(),"Error.Please try again",Toast.LENGTH_SHORT).show();

                            }


                        }catch(JSONException e) {

                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgress.setVisibility(View.GONE);

                VolleyLog.d("VolleyDebug",
                        "Error: " + error.getMessage());
               /* Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/
                Log.i("VolleyDebug", "Error: " + error.getMessage());

                if (error instanceof NetworkError) {

                    Toast.makeText(getApplicationContext(),
                            "Please check internet", Toast.LENGTH_SHORT).show();

                } else if (error instanceof ServerError) {

                    Toast.makeText(getApplicationContext(),
                            "ServerError", Toast.LENGTH_SHORT).show();

                } else if (error instanceof AuthFailureError) {

                    Toast.makeText(getApplicationContext(),
                            "AuthFailureError", Toast.LENGTH_SHORT)
                            .show();

                } else if (error instanceof ParseError) {

                    Toast.makeText(getApplicationContext(),
                            "ParseError", Toast.LENGTH_SHORT).show();

                } else if (error instanceof NoConnectionError) {

                    Toast.makeText(getApplicationContext(),
                            "NoConnectionError", Toast.LENGTH_SHORT)
                            .show();

                } else if (error instanceof TimeoutError) {

                    Toast.makeText(getApplicationContext(),
                            "TimeOutError", Toast.LENGTH_SHORT).show();

                }


            }
        });

        checkLoginRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(checkLoginRequest);
        Log.i("VolleyDebug", "Volley Object added to requst");

    }

    private void checkForCrashes() {
        CrashManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        checkForUpdates();
    }
}
