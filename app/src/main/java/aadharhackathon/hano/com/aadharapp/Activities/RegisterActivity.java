package aadharhackathon.hano.com.aadharapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONException;
import org.json.JSONObject;

import Util.Constants;
import aadharhackathon.hano.com.aadharapp.R;


public class RegisterActivity extends ActionBarActivity {

    ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        Button btRegister = (Button) findViewById(R.id.registerButton);
        final EditText etAdhaarNumber = (EditText) findViewById(R.id.aadharnumberEditText);
        final EditText etMobileNumber = (EditText) findViewById(R.id.mobileNumberEditText);
        final EditText etEmail = (EditText) findViewById(R.id.emailText);
        final EditText etPassword = (EditText) findViewById(R.id.passwordEditText);

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check for Aadhar NUmber
                if ((etAdhaarNumber.getText().toString().length() > 0) && (etAdhaarNumber.getText().toString().length() == 12)) {

                    //Check for Mobile number
                    if ((etMobileNumber.getText().toString().length() > 0) && etMobileNumber.getText().toString().length() == 10) {

                        //Check for Password
                        if (etPassword.getText().toString().trim().length() > 0) {

                            mProgress.setVisibility(View.VISIBLE);

                            try {

                                JSONObject registerObject = new JSONObject();
                                registerObject.put(Constants.REGISTER_AADHAAR, etAdhaarNumber.getText().toString());
                                registerObject.put(Constants.REGISTER_EMAIL, etEmail.getText().toString().trim());
                                registerObject.put(Constants.REGISTER_PASSWORD, etPassword.getText().toString());
                                registerObject.put(Constants.REGISTER_MOBILE, etMobileNumber.getText().toString());
                                registerObject.put(Constants.REGISTER_GENDER, "");
                                registerObject.put(Constants.REGISTER_NAME, "");
                                registerUser(registerObject, etAdhaarNumber.getText().toString());

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }


                        } else {

                            Toast.makeText(getApplicationContext(), "Please enter correct password.", Toast.LENGTH_SHORT).show();

                        }

                    } else {

                        Toast.makeText(getApplicationContext(), "Please enter 10 digit mobile no.", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(getApplicationContext(), "Please check Aadhar No.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void registerUser(JSONObject registerObject, final String aadharNumber) {

        //String url = "http://requestb.in/1aa9uyz1";
        String url = Constants.BASE_URL + Constants.DO_REGISTER;

        JSONObject testObject= registerObject;
        Log.i("test",testObject.toString());

        JsonObjectRequest registerUserRequest = new JsonObjectRequest(Request.Method.POST,url, registerObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        mProgress.setVisibility(View.GONE);
                        try {
                            String status = response.getString("success");
                            String message = response.getString("message");

                            if (status.equals("1")) {

                                Intent fingerPrintIntent = new Intent(RegisterActivity.this, FingerPrintScannerActivity.class);
                                fingerPrintIntent.putExtra(Constants.AADHAR_NUMBER, aadharNumber);
                                fingerPrintIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(fingerPrintIntent);
                                finish();

                            } else {

                                Toast.makeText(getApplicationContext(), "Error:"+message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);

                VolleyLog.d("VolleyDebug",
                        "Error: " + error.getMessage());
               /* Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/
                Log.i("VolleyDebug", "Error: " + error.getMessage());

                if (error instanceof NetworkError) {

                    Toast.makeText(getApplicationContext(),
                            "Please check internet", Toast.LENGTH_SHORT).show();

                } else if (error instanceof ServerError) {

                    Toast.makeText(getApplicationContext(),
                            "ServerError", Toast.LENGTH_SHORT).show();

                } else if (error instanceof AuthFailureError) {

                    Toast.makeText(getApplicationContext(),
                            "AuthFailureError", Toast.LENGTH_SHORT)
                            .show();

                } else if (error instanceof ParseError) {

                    Toast.makeText(getApplicationContext(),
                            "ParseError", Toast.LENGTH_SHORT).show();

                } else if (error instanceof NoConnectionError) {

                    Toast.makeText(getApplicationContext(),
                            "NoConnectionError", Toast.LENGTH_SHORT)
                            .show();

                } else if (error instanceof TimeoutError) {

                    Toast.makeText(getApplicationContext(),
                            "TimeOutError", Toast.LENGTH_SHORT).show();

                }


            }
        });

        registerUserRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(registerUserRequest);
        Log.i("VolleyDebug", "Volley Object added to requst");

    }

    private void checkForCrashes() {
        CrashManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, "678ef956e2257f26929f724c31c45485");
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        checkForUpdates();
    }
}
